<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/customers/{id?}', function ($id = null) {
    if($id != null) {
    return view('pages.customers', ['id' => $id ]); }
    else {
    echo "No customer was provided" ; }
    }); 

Route::resource('todos','TodoController'); 

Route::resource('books','BookController'); 