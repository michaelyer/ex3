<?php

use Illuminate\Database\Seeder;

class BookTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->insert([
            [
                'title' => 'Harry Potter and the Sorcerers Stone',
                'author' => 'J.K. Rowling',
                'created_at' => date('Y-m-d G:i:s')
            ],
            [
                'title' => 'The Diary of a Young Girl',
                'author' => 'Anne Frank',
                'created_at' => date('Y-m-d G:i:s')
            ],
            [
                'title' => 'A Game of Thrones (A Song of Ice and Fire, #1)',
                'author' => 'George R.R. Martin',
                'created_at' => date('Y-m-d G:i:s')
            ],
            [
                'title' => 'The Chronicles of Narnia (Chronicles of Narnia, #1-7)',
                'author' => 'C.S. Lewis',
                'created_at' => date('Y-m-d G:i:s')
            ],
            [
                'title' => 'The Story of Life',
                'author' => 'Chris (Simpsons Artist)',
                'created_at' => date('Y-m-d G:i:s')
            ],
            [
                'title' => 'Arsen: A Broken Love Story',
                'author' => 'Mia Asher',
                'created_at' => date('Y-m-d G:i:s')
            ]
           
        ]);
    }
}
